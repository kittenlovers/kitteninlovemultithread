package absyn;

import java.io.FileWriter;

import semantical.TypeChecker;
import translation.Block;
import types.ClassType;
import types.CodeSignature;
import bytecode.ERRORSTRING;
import bytecode.RETURN;

/**
 * @author kittenLovers
 *
 */
public class Assert extends Command {

	/**
	 * The expression of the assert.
	 */
	private final Expression expression;

	/**
	 * Yields the error string of the Assert in format " at row::column".
	 */
	private String errorString;

	/**
	 * Constructs the abstract syntax of a {@code assert} command.
	 *
	 * @param pos
	 *            the position in the source file where it starts the concrete
	 *            syntax represented by this abstract syntax
	 * @param expression
	 *            the condition of the assert.
	 */
	public Assert(int pos, Expression expression) {
		super(pos);
		this.expression = expression;
		this.errorString = "";
	}

	/**
	 * Yields the condition or guard of the assert.
	 *
	 * @return the condition or guard of the assert.
	 */

	public Expression getCondition() {
		return expression;
	}

	/**
	 * Performs the type-checking of the assert command by using a given
	 * type-checker. It type-checks the expression. It checks that the condition
	 * is a Boolean expression. It returns the original type-checker passed as a
	 * parameter.
	 *
	 * @param checker
	 *            the type-checker to be used for type-checking
	 * @return the type-checker {@code checker}
	 */
	@Override
	protected TypeChecker typeCheckAux(TypeChecker checker) {
		if (!checker.isAssertAllowed())
			error("assert is an invalid statement in this member");
		int pos = getPos();

		this.errorString = checker.getAssertError(pos, "\ttest fallito ");

		expression.mustBeBoolean(checker);

		// check that the expression static type
		expression.typeCheck(checker);
		return checker;
	}

	/**
	 * Checks that this command does not contain <i>dead-code</i>, that is,
	 * commands which can never be executed. This is always true for the skip
	 * command.
	 *
	 * @return false, since this command never terminates with a {@code return}
	 */
	@Override
	public boolean checkForDeadcode() {
		return false;
	}

	/**
	 * Adds abstract syntax class-specific information in the dot file
	 * representing the abstract syntax of the assert command. This amounts to
	 * adding arc from the node for the assert command to the abstract syntax
	 * for its {@link #condition}.
	 *
	 * @param where
	 *            the file where the dot representation must be written
	 */

	@Override
	protected void toDotAux(FileWriter where) throws java.io.IOException {
		linkToNode("Assert", getExpression().toDot(where), where);
	}

	/**
	 * @return the expression
	 */
	public Expression getExpression() {
		return expression;
	}

	/**
	 * Translates this command into intermediate Kitten bytecode. It returns a
	 * code which evaluates the [@link #condition} of the conditional and if the
	 * assert fails, it continues with the return of the errorString else it
	 * continues with {@code continuation}.
	 *
	 * @param where
	 *            the method or constructor where this expression occurs
	 * @param continuation
	 *            the continuation to be executed after this command
	 * @return the code executing this command and then the {@code continuation}
	 */
	@Override
	public Block translate(CodeSignature where, Block continuation) {

		ClassType string = ClassType.mk("String");
		continuation.doNotMerge();

		// build a java.lang.String with ERRORSTRING bytecode and return it
		Block no = new ERRORSTRING(errorString).followedBy(new Block(
				new RETURN(string)));

		no.doNotMerge();

		Block block = expression.translateAsTest(where, continuation, no);

		return block;
	}
}
