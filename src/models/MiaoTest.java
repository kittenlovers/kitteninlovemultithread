package models;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MiaoTest {

	private static void fixture0(Miao m) {
		m.i = 10;
	}

	private static void fixture1(Miao m) {
		m.i = 20;
	}

	private static void fixture2(Miao m) {
		m.i = 30;
	}

	protected static String test1() {
		Miao m = new Miao();
		fixture0(m);
		fixture1(m);
		fixture2(m);
		m.i = 30;
		if (m.i > 10) {// assertCondition
			if (m.i > 20) { // assertCondition
				System.out.println("ciccio");
				return null;
			} else {
				return new runTime.String("7::5").toString();
			}
		} else {
			return new runTime.String("6::5").toString();
		}
	}

	protected static String test2() {
		Miao m = new Miao();
		fixture0(m);
		fixture1(m);
		fixture2(m);
		m.i = 30;
		if (m.i > 40) { // assertCondition
			if (m.i > 50) { // assertCondition
				System.out.println("pluto");
				return null;
			} else {
				return new runTime.String("7::5").toString();
			}
		} else {
			return new runTime.String("6::5").toString();
			// return "6::5";
		}
	}

	public static void main(String args[]) {
		System.out.println("Executing tests..");
		int passedTests = 0, failedTest = 0;
		String textResult = null, positionError = null;
		StringBuilder outBuilder = new StringBuilder("\nResults:\n");
		long testsTimeStart = System.nanoTime();

		ThreadPoolExecutor pool = (ThreadPoolExecutor) Executors
				.newCachedThreadPool();

		Set<Future<Pair<Long, Pair<String, String>>>> set = new HashSet<>();

		Callable<Pair<Long, Pair<String, String>>> callable = null; // 11
		callable = new ThreadTest("test1", "models.MiaoTest");
		Future<Pair<Long, Pair<String, String>>> ft = pool.submit(callable);
		set.add(ft);

		callable = new ThreadTest("test2", "models.MiaoTest");
		ft = pool.submit(callable);
		set.add(ft);

		for (Future<Pair<Long, Pair<String, String>>> future : set) {
			try {
				if (future.get().getRight().getRight() == null) {
					passedTests++;
					textResult = "passed [";
					positionError = "";
				} else {
					failedTest++;
					textResult = "failed [";
					positionError = future.get().getRight().getRight();
				}
				outBuilder.append("\n\t-")
						.append(future.get().getRight().getLeft()).append(": ")
						.append(textResult)
						.append(getMsF(future.get().getLeft())).append("ms] ")
						.append(positionError);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}

		System.out.println(outBuilder.append("\n\n").append(passedTests)
				.append(" tests passed, ").append(failedTest)
				.append(" failed [")
				.append(getMsF(System.nanoTime() - testsTimeStart))
				.append("ms] ").toString());

	}

	private static double getMsF(long nanoseconds) {
		return TimeUnit.NANOSECONDS.toMicros(nanoseconds) / 1000.000;
	}
}
