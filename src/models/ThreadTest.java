package models;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public class ThreadTest implements Callable<Pair<Long, Pair<String, String>>> {

	private String test;
	private String clazz;
	private final int number;
	private static int counter = 0;

	public ThreadTest(String test, String clazz) {
		this.test = test;
		this.clazz = clazz;
		synchronized (this) { // questo non siamo riusciti a tradurlo
			this.number = counter++;
		}
	}

	public Pair<Long, Pair<String, String>> call() {
		System.out.println("hi im the thread: " + number);
		long testTimeStart = System.nanoTime();
		// execute test
		String result = "test not found";
		try {
			Method method = Class.forName(clazz).getDeclaredMethod(test,
					new Class[0]);
			result = (String) method.invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return new Pair<Long, Pair<String, String>>(System.nanoTime()
				- testTimeStart, new Pair<String, String>(test, result));
	}
}