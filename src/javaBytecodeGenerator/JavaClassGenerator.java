package javaBytecodeGenerator;

import java.util.Set;

import org.apache.bcel.Constants;

import types.ClassMemberSignature;
import types.ClassType;
import types.ConstructorSignature;
import types.FieldSignature;
import types.MethodSignature;

/**
 * @author blasco991
 *
 */
@SuppressWarnings("serial")
public class JavaClassGenerator extends ClassGenerator {

	/**
	 * Builds a class generator for the given class type.
	 *
	 * @param clazz
	 *            the class type
	 * @param sigs
	 *            a set of class member signatures. These are those that must be
	 *            translated. If this is {@code null}, all class members are
	 *            translated
	 */

	public JavaClassGenerator(ClassType clazz, Set<ClassMemberSignature> sigs) {
		super(clazz.getName(), // nameclass
				// the superclass of the Kitten Object class is set to be the
				// Java java.lang.Object class
				clazz.getSuperclass() != null ? clazz.getSuperclass().getName()
						: "java.lang.Object", Constants.ACC_PUBLIC); // Java
																		// attributes:
																		// public!

		// we add the fields
		for (FieldSignature field : clazz.getFields().values())
			if (sigs == null || sigs.contains(field))
				field.createField(this);

		// we add the constructors
		for (ConstructorSignature constructor : clazz.getConstructors())
			if (sigs == null || sigs.contains(constructor))
				constructor.createConstructor(this);

		// we add the methods
		for (Set<MethodSignature> s : clazz.getMethods().values())
			for (MethodSignature method : s)
				if (sigs == null || sigs.contains(method))
					method.createMethod(this);

	}

}
