package javaBytecodeGenerator;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import models.Pair;
import models.ThreadTest;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.BranchHandle;
import org.apache.bcel.generic.CHECKCAST;
import org.apache.bcel.generic.DDIV;
import org.apache.bcel.generic.GOTO;
import org.apache.bcel.generic.IFNE;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IINC;
import org.apache.bcel.generic.InstructionConstants;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2D;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC2_W;
import org.apache.bcel.generic.LSUB;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;

import types.ClassMemberSignature;
import types.ClassType;
import types.FixtureSignature;
import types.TestSignature;

/**
 * @author kittenlovers
 *
 */
@SuppressWarnings("serial")
public class TestClassGenerator extends ClassGenerator {

	/**
	 * Builds a test class generator for the given class type.
	 *
	 * @param clazz
	 *            the class type
	 * @param sigs
	 *            a set of class member signatures. These are those that must be
	 *            translated. If this is {@code null}, all class members are
	 *            translated
	 */

	public TestClassGenerator(ClassType clazz, Set<ClassMemberSignature> sigs) {
		super(clazz.getName() + "Test", // classname
				// the superclass of the Kitten Object class is set to be the
				// Java java.lang.Object class
				clazz.getSuperclass() != null ? clazz.getSuperclass().getName()
						: "java.lang.Object", Constants.ACC_PUBLIC);

		// we add the fixtures
		for (FixtureSignature fixture : clazz.getFixtures())
			if (sigs == null || sigs.contains(fixture))
				fixture.createFixture(this);

		// we add the tests
		for (TestSignature test : clazz.getTests())
			if (sigs == null || sigs.contains(test))
				test.createTest(this, clazz.getFixtures());

		final Type sBType = Type.getType(StringBuilder.class);
		final String sBName = StringBuilder.class.getName();
		final Type[] arg1Str = { Type.STRING };
		final String arg_name[] = { "args" };
		final short ikvt = Constants.INVOKEVIRTUAL;

		InstructionList il = new InstructionList();

		MethodGen methodMain = new MethodGen(Constants.ACC_PUBLIC
				| Constants.ACC_STATIC, Type.VOID, new Type[] { new ArrayType(
				"java.lang.String", 1) }, arg_name, "main", getClassName(), il,
				getConstantPool());

		il.append(getFactory().createPrintln("\n\nExecuting tests.."));

		il.append(InstructionFactory.ICONST_0);
		il.append(InstructionFactory.ISTORE_1);
		il.append(InstructionFactory.ICONST_0);
		il.append(InstructionFactory.ISTORE_2);

		il.append(InstructionFactory.ACONST_NULL);
		il.append(InstructionFactory.createStore(Type.NULL, 3));
		il.append(InstructionFactory.ACONST_NULL);
		il.append(InstructionFactory.createStore(Type.NULL, 4));

		il.append(getFactory().createNew(sBName));
		il.append(InstructionConstants.DUP);
		il.append(new LDC(getConstantPool().addString("\nResults: ")));
		il.append(getFactory().createInvoke(sBName, Constants.CONSTRUCTOR_NAME,
				Type.VOID, arg1Str, Constants.INVOKESPECIAL));
		il.append(InstructionFactory.createStore(sBType, 5));

		il.append(getFactory().createInvoke(System.class.getName(), "nanoTime",
				Type.LONG, Type.NO_ARGS, Constants.INVOKESTATIC));
		il.append(InstructionFactory.createStore(Type.LONG, 6));

		il.append(getFactory().createInvoke(Executors.class.getName(),
				"newCachedThreadPool", Type.getType(ExecutorService.class),
				Type.NO_ARGS, Constants.INVOKESTATIC));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(ThreadPoolExecutor.class))));
		il.append(InstructionFactory.createStore(
				Type.getType(ThreadPoolExecutor.class), 8));

		il.append(getFactory().createNew(
				(ObjectType) Type.getType(HashSet.class)));
		il.append(InstructionConstants.DUP);
		il.append(getFactory().createInvoke(HashSet.class.getName(),
				Constants.CONSTRUCTOR_NAME, Type.VOID, Type.NO_ARGS,
				Constants.INVOKESPECIAL));
		il.append(InstructionFactory.createStore(Type.getType(HashSet.class), 9));

		il.append(InstructionFactory.ACONST_NULL);
		il.append(InstructionFactory.createStore(Type.getType(Callable.class),
				10));

		for (TestSignature testSignature : clazz.getTests()) {

			il.append(getFactory().createNew(
					(ObjectType) Type.getType(ThreadTest.class)));
			il.append(InstructionConstants.DUP);
			il.append(new LDC(getConstantPool().addString(
					testSignature.getName())));
			il.append(new LDC(getConstantPool().addString(getClassName())));
			il.append(getFactory().createInvoke(ThreadTest.class.getName(),
					Constants.CONSTRUCTOR_NAME, Type.VOID,
					new Type[] { Type.STRING, Type.STRING },
					Constants.INVOKESPECIAL));
			il.append(InstructionFactory.createStore(
					Type.getType(Callable.class), 10));

			il.append(InstructionFactory.createLoad(
					Type.getType(ThreadPoolExecutor.class), 8));
			il.append(InstructionFactory.createLoad(
					Type.getType(Callable.class), 10));
			il.append(getFactory().createInvoke(
					ThreadPoolExecutor.class.getName(), "submit",
					Type.getType(Future.class),
					new Type[] { Type.getType(Callable.class) }, ikvt));
			il.append(InstructionFactory.createStore(
					Type.getType(Future.class), 11));
			il.append(InstructionFactory.createLoad(
					Type.getType(HashSet.class), 9));
			il.append(InstructionFactory.createLoad(Type.getType(Future.class),
					11));
			il.append(getFactory().createInvoke(Set.class.getName(), "add",
					Type.BOOLEAN, new Type[] { Type.OBJECT },
					Constants.INVOKEINTERFACE));
			il.append(InstructionConstants.POP);
		}

		// foreach
		il.append(InstructionFactory.createLoad(Type.getType(HashSet.class), 9));
		il.append(getFactory().createInvoke(Set.class.getName(), "iterator",
				Type.getType(Iterator.class), Type.NO_ARGS,
				Constants.INVOKEINTERFACE));
		il.append(InstructionFactory.createStore(Type.getType(Iterator.class),
				13));
		BranchHandle foreachGoTo = il.append(new GOTO(null));
		InstructionHandle cycleForEach = il.append(InstructionFactory
				.createLoad(Type.getType(Iterator.class), 13));
		il.append(getFactory().createInvoke(Iterator.class.getName(), "next",
				Type.OBJECT, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Future.class))));
		il.append(InstructionFactory.createStore(Type.getType(Future.class), 12));

		// bodyforeach
		il.append(InstructionFactory.createLoad(Type.getType(Future.class), 12));
		il.append(getFactory().createInvoke(Future.class.getName(), "get",
				Type.OBJECT, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Pair.class))));
		il.append(getFactory().createInvoke(Pair.class.getName(), "getRight",
				Type.OBJECT, Type.NO_ARGS, ikvt));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Pair.class))));
		il.append(getFactory().createInvoke(Pair.class.getName(), "getRight",
				Type.OBJECT, Type.NO_ARGS, ikvt));
		BranchHandle ifNotNull = il.append(new IFNONNULL(null));

		// then
		il.append(new IINC(1, 1));
		il.append(new LDC(getConstantPool().addString("passed [")));
		il.append(InstructionFactory.createStore(Type.STRING, 3));
		il.append(new LDC(getConstantPool().addString("")));
		il.append(InstructionFactory.createStore(Type.STRING, 4));
		BranchHandle notNullGoTo = il.append(new GOTO(null));

		// else
		ifNotNull.setTarget(il.append(new IINC(2, 1)));
		il.append(new LDC(getConstantPool().addString("failed [")));
		il.append(InstructionFactory.createStore(Type.STRING, 3));
		il.append(InstructionFactory.createLoad(Type.getType(Future.class), 12));
		il.append(getFactory().createInvoke(Future.class.getName(), "get",
				Type.OBJECT, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Pair.class))));
		il.append(getFactory().createInvoke(Pair.class.getName(), "getRight",
				Type.OBJECT, Type.NO_ARGS, ikvt));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Pair.class))));
		il.append(getFactory().createInvoke(Pair.class.getName(), "getRight",
				Type.OBJECT, Type.NO_ARGS, ikvt));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.STRING)));
		il.append(InstructionFactory.createStore(Type.STRING, 4));
		// fine else

		notNullGoTo.setTarget(il.append(InstructionFactory
				.createLoad(sBType, 5)));
		il.append(new LDC(getConstantPool().addString("\n\t-")));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(InstructionFactory.createLoad(Type.getType(Future.class), 12));
		il.append(getFactory().createInvoke(Future.class.getName(), "get",
				Type.OBJECT, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Pair.class))));
		il.append(getFactory().createInvoke(Pair.class.getName(), "getRight",
				Type.OBJECT, Type.NO_ARGS, ikvt));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Pair.class))));
		il.append(getFactory().createInvoke(Pair.class.getName(), "getLeft",
				Type.OBJECT, Type.NO_ARGS, ikvt));
		il.append(new CHECKCAST(getConstantPool().addClass(Type.STRING)));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(new LDC(getConstantPool().addString(": ")));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(InstructionFactory.createLoad(Type.STRING, 3));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(InstructionFactory.createLoad(Type.getType(Future.class), 12));
		il.append(getFactory().createInvoke(Future.class.getName(), "get",
				Type.OBJECT, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(new CHECKCAST(getConstantPool().addClass(
				(ObjectType) Type.getType(Pair.class))));
		il.append(getFactory().createInvoke(Pair.class.getName(), "getLeft",
				Type.OBJECT, Type.NO_ARGS, ikvt));
		il.append(new CHECKCAST(getConstantPool()
				.addClass(Long.class.getName())));
		il.append(getFactory().createInvoke(Long.class.getName(), "longValue",
				Type.LONG, Type.NO_ARGS, ikvt));
		il.append(getFactory().createInvoke(getClassName(), "getMsF",
				Type.DOUBLE, new Type[] { Type.LONG }, Constants.INVOKESTATIC));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.DOUBLE }, ikvt));
		il.append(new LDC(getConstantPool().addString("ms] ")));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(InstructionFactory.createLoad(Type.STRING, 4));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(InstructionConstants.POP);
		foreachGoTo.setTarget(il.append(InstructionFactory.createLoad(
				Type.getType(Iterator.class), 13)));
		il.append(getFactory().createInvoke(Iterator.class.getName(),
				"hasNext", Type.BOOLEAN, Type.NO_ARGS,
				Constants.INVOKEINTERFACE));
		il.append(new IFNE(cycleForEach));

		il.append(getFactory().createGetStatic(System.class.getName(), "out",
				Type.getType(PrintStream.class)));
		il.append(InstructionFactory.createLoad(sBType, 5));
		il.append(new LDC(getConstantPool().addString("\n\n")));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(InstructionFactory.ILOAD_1);
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.INT }, ikvt));
		il.append(new LDC(getConstantPool().addString(" tests passed, ")));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));
		il.append(InstructionFactory.ILOAD_2);
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.INT }, ikvt));
		il.append(new LDC(getConstantPool().addString(" failed [")));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.STRING }, ikvt));

		// salvo nanoTime
		il.append(getFactory().createInvoke(System.class.getName(), "nanoTime",
				Type.LONG, Type.NO_ARGS, Constants.INVOKESTATIC));
		il.append(InstructionFactory.createLoad(Type.LONG, 6));
		il.append(new LSUB());
		il.append(getFactory().createInvoke(getClassName(), "getMsF",
				Type.DOUBLE, new Type[] { Type.LONG }, Constants.INVOKESTATIC));
		il.append(getFactory().createInvoke(sBName, "append", sBType,
				new Type[] { Type.DOUBLE }, ikvt));
		il.append(new LDC(getConstantPool().addString("ms]")));
		il.append(getFactory().createInvoke(sBName, "append", sBType, arg1Str,
				ikvt));
		il.append(getFactory().createInvoke(sBName, "toString", Type.STRING,

		Type.NO_ARGS, ikvt));
		il.append(getFactory().createInvoke(PrintStream.class.getName(),
				"println", Type.VOID, arg1Str, ikvt));

		il.append(InstructionConstants.RETURN);

		methodMain.setMaxStack();
		methodMain.setMaxLocals();

		// we add a method to the class that we are generating
		addMethod(methodMain.getMethod());

		InstructionList iM = new InstructionList();

		iM.append(getFactory().createGetStatic(TimeUnit.class.getName(),
				"NANOSECONDS", Type.getType(TimeUnit.class)));
		iM.append(InstructionFactory.createLoad(Type.LONG, 0));
		iM.append(getFactory().createInvoke(TimeUnit.class.getName(),
				"toMicros", Type.LONG, new Type[] { Type.LONG }, ikvt));
		iM.append(new L2D());
		int index = getConstantPool().addDouble(1000);
		iM.append(new LDC2_W(index));
		iM.append(new DDIV());
		iM.append(InstructionFactory.createReturn(Type.DOUBLE));

		MethodGen methodgetMsF = new MethodGen(Constants.ACC_PRIVATE
				| Constants.ACC_STATIC, Type.DOUBLE, new Type[] { Type.LONG },
				arg_name, "getMsF", getClassName(), iM, getConstantPool());

		methodgetMsF.setMaxStack();
		methodgetMsF.setMaxLocals();

		// we add a method to the class that we are generating
		addMethod(methodgetMsF.getMethod());

	}
}
