package types;

import java.util.Set;
import javaBytecodeGenerator.TestClassGenerator;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.InstructionConstants;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import translation.Block;
import absyn.TestDeclaration;

public class TestSignature extends CodeSignature {

	/**
	 * Constructs the signature of a test with the given name.
	 *
	 * @param clazz
	 *            the class where this method is defined
	 * @param name
	 *            the name of the method
	 * @param abstractSyntax
	 *            the abstract syntax of the declaration of this method
	 */

	public TestSignature(ClassType clazz, String name,
			TestDeclaration abstractSyntax) {

		super(clazz, VoidType.INSTANCE, TypeList.EMPTY, name, abstractSyntax);
	}

	/**
	 * Adds to the given class generator a Java bytecode method for this test.
	 *
	 * @param classGen
	 *            the generator of the class where the test lives
	 */
	public void createTest(TestClassGenerator testClassGenerator,
			Set<FixtureSignature> fixtures) {
		InstructionList instructionList = new InstructionList();

		instructionList.append(testClassGenerator.getFactory().createNew(
				getDefiningClass().getName()));
		instructionList.append(InstructionConstants.DUP);
		instructionList.append(testClassGenerator.getFactory().createInvoke(
				getDefiningClass().getName(), // class name of the method
				Constants.CONSTRUCTOR_NAME, // name of the method
				Type.VOID, // return type
				Type.NO_ARGS, // type parameters
				Constants.INVOKESPECIAL)); // invokespecial
		instructionList.append(InstructionFactory.ASTORE_0);

		for (FixtureSignature fixtureSignature : fixtures) {
			instructionList.append(InstructionConstants.ALOAD_0);
			instructionList.append(testClassGenerator.getFactory()
					.createInvoke(testClassGenerator.getClassName(),
							fixtureSignature.getName(), Type.VOID,
							new Type[] { (getDefiningClass()).toBCEL() },
							Constants.INVOKESTATIC));
		}

		instructionList.append(testClassGenerator
				.generateJavaBytecode(getCode()));

		MethodGen methodGen = new MethodGen(Constants.ACC_PUBLIC
				| Constants.ACC_STATIC, Type.STRING, Type.NO_ARGS, null,
				getName(), testClassGenerator.getClassName(), instructionList,
				testClassGenerator.getConstantPool());

		// we must always call these methods before the getMethod()
		// method below. They set the number of local variables and stack
		// elements used by the code of the method
		methodGen.setMaxStack();
		methodGen.setMaxLocals();

		// we add a method to the class that we are generating
		testClassGenerator.addMethod(methodGen.getMethod());

	}

	@Override
	protected Block addPrefixToCode(Block code) {
		// we prefix a piece of code that calls the constructor of
		return code;
	}

}
